package com.marcurytoursnew.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

//This is a test message to check bit bucket
public class MethodRepository {
	static WebDriver driver;

	// App launch method
	public static void appLanumch() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
	}

	public static void verifiedValidLogin() throws AWTException, FindFailed, InterruptedException, IOException {
		// WebElement uname = driver.findElement(By.name("userName"));
		WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));
		// uname.sendKeys("dasd");
		// property file reading
		Properties obj = new Properties();
		FileInputStream objfile = new FileInputStream(System.getProperty("./TestData/testdata.properties"));
		obj.load(objfile);
		String uname1 = obj.getProperty("username");
		uname.sendKeys(uname1);

		// Thread.sleep(3000);
		//// SIKULI IMPLEMENTATION
		// Screen screen = new Screen();
		// Thread.sleep(3000);
		// Pattern username = new Pattern("./SikuliImage/username1.png");
		// screen.wait(username,10);
		// screen.type(username,"dasd");

		WebElement pass = driver.findElement(By.name("password"));
		pass.sendKeys("dasd");
		// WebElement signin = driver.findElement(By.name("login"));
		// signin.click();

		/// ACTION CLASS IMPLEMENTATION
		WebElement signin = driver.findElement(By.name("login"));
		Actions builder = new Actions(driver);
		builder.moveToElement(signin).click().build().perform();

		// Robot class implementation
		/*
		 * Robot btnenter = new Robot(); btnenter.keyPress(KeyEvent.VK_ENTER);
		 * btnenter.keyRelease(KeyEvent.VK_ENTER);
		 */
		// Robot class end

		String exptitle = "Find a Flight: Mercury Tours:";
		String acttitle = driver.getTitle();
		if (exptitle.equals(acttitle)) {
			System.out.println("Login Successfull");
		} else {
			System.out.println("Login is ");
		}
	}

	public static void verifiedInvalidLogin() {
		// WebElement uname = driver.findElement(By.name("userName"));
		WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys("dad");
		WebElement pass = driver.findElement(By.name("password"));
		pass.sendKeys("12345678");
		WebElement signin = driver.findElement(By.name("login"));
		signin.click();
		// or not equal to previous text

		String exptitle = "Sign-on: Mercury Tours";
		String acttitle = driver.getTitle();
		if (exptitle.equals(acttitle)) {
			System.out.println("Login UnSuccessfull");
		} else {
			System.out.println("Login is ");
		}
	}

	public static void verifyPassangers() {
		WebElement dropdown = driver.findElement(By.name("fromPort"));
		Select departFrom = new Select(dropdown);
		departFrom.selectByVisibleText("London");
		departFrom.selectByIndex(3);
		departFrom.selectByValue("Paris");
	}

	// AutoIT Implementation for file upload
	public static void verifyFileUpload() throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://files.fm/");
		// Thread.sleep(2000);
		WebElement btnuploadpic = driver.findElement(By.id("uploadifive-file_upload"));
		btnuploadpic.click();
		Thread.sleep(3000);

		Runtime.getRuntime().exec("./AutoITScript/fileuploadmethod.exe");
		Thread.sleep(3000);
		WebElement startuploadpic = driver.findElement(By.id("savefiles"));
		startuploadpic.click();
	}

	public static void verifyTestFlight() {

	}

	public static void webTablehandling() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://money.rediff.com/gainers/bsc/daily/groupa");
		List col = driver.findElements(By.xpath(".//*[@id=\"leftcontainer\"]/table/thead/tr/th"));
		System.out.println("No of cols are : " + col.size());
		List row = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
		System.out.println("No of rows are : " + col.size());
	}

	public static void browserClose() {
		driver.close();
	}
}