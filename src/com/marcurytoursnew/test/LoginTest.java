package com.marcurytoursnew.test;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public final class LoginTest {
	WebDriver driver;

	/*@BeforeMethod
	public void browserAppLaunch() {
		MethodRepository.appLanumch();
	}*/

	@Test(priority=1,enabled=false)
	public void verifiedValidLogin() throws FindFailed, AWTException, InterruptedException, IOException {
		MethodRepository.verifiedValidLogin();
	}
	@Test(priority=2,enabled=false)
	public void verifiedInValidLogin() throws FindFailed, AWTException, InterruptedException {
		MethodRepository.verifiedInvalidLogin();
	}
	@Test(priority=1,enabled=true)
	public void webTablehandling() {
		MethodRepository.webTablehandling();
	}

	@AfterMethod
	public void cleanUp() {
		{
			MethodRepository.browserClose();
		}
	}
}